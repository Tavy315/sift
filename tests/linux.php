<?php

function execMatches($keyFile1, $keyFile2)
{
    $result = exec('./matches -k1 ' . $keyFile1 . ' -k2 ' . $keyFile2 . ' 2>&1');

    return [
        'file1'  => $keyFile1,
        'file2'  => $keyFile2,
        'result' => $result,
    ];

    /*$matches = [ ];
    preg_match('/^Found (.*) matches\.$/', $result, $matches);

    if (count($matches) == 2) {
        return $matches[1];
    }

    return 0;*/
}

$matches = [ ];
for ($i = 1; $i < 4; $i++) {
    $matches[$i] = execMatches('0.key', $i . '.key');
}
var_dump($matches);
