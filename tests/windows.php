<?php

include 'sift/Matcher.php';
include 'sift/Util.php';

/**
 * @link other stuff on http://stackoverflow.com/questions/16487606/ignore-comments-of-sql-in-php-when-reading-a-sql-file-in-php
 */

$util = new \Sift\Util();

//$image0 = $util->readPGMFile('0.pgm');
//$image2 = $util->readPGMFile('2.pgm');
$keypoints0 = $util->readKeys('0.txt');
//$keypoints1 = $util->readKeys('1.txt');
$keypoints2 = $util->readKeys('2.txt');
$matcher = new \Sift\Matcher();
$matcher->calculateMatches($keypoints2, $keypoints0);
//$matcher->findMatches($image0, $keypoints0, $image2, $keypoints2);
//$util->writePGMFile('out.pgm', $image0);
