<?php
namespace Sift;

include 'sift/Image.php';
include 'sift/Keypoint.php';
include 'sift/PGM.php';

/**
 * Class Util
 *
 * @package Sift
 * @author  Octavian Matei <octav@octav.name>
 */
class Util
{
    /**
     * Create a new image with uninitialized pixel values.
     *
     * @param int $rows
     * @param int $cols
     *
     * @return Image
     */
    public function createImage($rows, $cols)
    {
        $im = new Image();
//        $im = (Image) malloc(sizeof(struct ImageSt));
        $im->rows = $rows;
        $im->cols = $cols;
//        $im->pixels = $this->allocMatrix($rows, $cols);
        $im->pixels = [ [ ] ];
        $im->next = null;

        return $im;
    }

    /**
     * Allocate memory for a 2D float matrix of size [row,col].
     * This returns a vector of pointers to the rows of the matrix,
     * so that routines can operate on this without knowing the dimensions.
     *
     * @param int $rows
     * @param int $cols
     *
     * @return float
     */
    function allocMatrix($rows, $cols)
    {
//        $m = (float **) malloc($rows * sizeof(float *));
//        $v = (float *) malloc($rows * $cols * sizeof(float));
//        $m = floatval($rows * PHP_INT_SIZE);
        $v = floatval($rows * $cols * PHP_INT_SIZE);
        $m = [ ];
        for ($i = 0; $i < $rows; $i++) {
            $m[$i] = $v;
            $v += $cols;
        }

        return $m;
    }

    /**
     * This reads a keypoint file from a given filename and returns the list of keypoints.
     *
     * @param string $filename
     *
     * @return Keypoint
     * @throws \Exception
     */
    public function readKeyFile($filename)
    {
        /*$file = fopen($filename, 'r');
        if (!$file) {
            throw new \Exception('Could not open file: ' . $filename, 404);
        }*/

        return $this->readKeys($filename);
    }

    /**
     * Read keypoints from the given file pointer and return the list of keypoints.
     * The file format starts with 2 integers giving the total number of keypoints
     * and the size of descriptor vector for each keypoint (currently assumed to be 128).
     *
     * Then each keypoint is specified by 4 floating point numbers giving subpixel row
     * and column location, scale, and orientation (in radians from -PI to PI).
     *
     * Then the descriptor vector for each keypoint is given as a list of integers in range [0,255].
     *
     * @param $filename
     *
     * @return Keypoint $keys
     * @throws \Exception
     */
    public function readKeys($filename)
    {
        $lines = file($filename, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        $params = explode(' ', current($lines));
        if (count($params) != 2) {
            throw new \Exception('Invalid keypoint file beginning.', 400);
        }
        list($num, $len) = $params;

        /*$result = fscanf($fp, "%d %d");
        if (count($result) != 2) {
            throw new \Exception('Invalid keypoint file beginning.', 400);
        }

        list($num, $len) = $result;*/

        if ($len != 128) {
            throw new \Exception('Keypoint descriptor length invalid (should be 128).', 400);
        }

        $keys = null;
//        $file = fread($fp, 81920);
//        $lines = explode("\n", $file);

        next($lines);

        for ($i = 0; $i < $num; $i++) {
            $k = new Keypoint();
            $k->setNext($keys);
            $keys = $k;

            $params = explode(' ', current($lines));
            if (count($params) == 4) {
                list($k->row, $k->col, $k->scale, $k->ori) = $params;
            } else {
                die('error: not 4 params');
            }
            next($lines);
            $j = 0;
            while ($j < $len) {
                $line = (explode(' ', trim(current($lines))));
                foreach ($line as $val) {
                    $k->setDescriptors($j++, $val);
                }
                next($lines);
            }
        }
        /*
        $j = 0;
        foreach ($lines as $line) {
            if ($line == '') {
                continue;
            }
            if ($line[0] !== ' ') {
                // Allocate memory for the keypoint.
                $k = new Keypoint();
                $k->setNext($keys);
                $keys = $k;
                list($k->row, $k->col, $k->scale, $k->ori) = explode(' ', trim($line));
                $j = 0;
            } else {
                $line = (explode(' ', trim($line)));
                foreach ($line as $val) {
                    $k->setDescriptors($j, $val);
                    $j++;
                }
            }
        }*/

        /*
                for ($i = 0; $i < $num; $i++) {
                    $k = new Keypoint();
                    $k->setNext($keys);
                    $keys = $k;
                    $line = fscanf($fp, "%f %f %f %f");
                    if (count($line) != 4) {
                        throw new \Exception('Invalid keypoint file format.', 400);
                    }
                    list($k->row, $k->col, $k->scale, $k->ori) = $line;

                    for ($j = 0; $j < $len; $j++) {
                        $line = fscanf($fp, "%d");
                        if (count($line) != 1) {
                            throw new \Exception("Invalid keypoint file value.", 400);
                        }
                        list($val) = $line;
                        echo $val . PHP_EOL;
                        if ($val < 0 || $val > 255) {
                            throw new \Exception("Invalid keypoint file value.", 400);
                        }
        //                $k->descriptor[$j] = (unsigned char) $val;
                    }
                }*/

        return ($keys);
    }

    /**
     * This reads a PGM file from a given filename and returns the image.
     *
     * @param resource $filename
     *
     * @return Image
     * @throws \Exception
     */
    public function readPGMFile($filename)
    {
        $pgm = new PGM();
        $pgm->loadPGM($filename);

        $image = $this->createImage($pgm->height, $pgm->width);

        $i = 0;
        for ($r = 0; $r < $pgm->height; $r++) {
            for ($c = 0; $c < $pgm->width; $c++) {
                if (!isset($image->pixels[$r])) {
                    $image->pixels[$r] = [ ];
                }
                if (!isset($image->pixels[$r][$c])) {
                    $image->pixels[$r][$c] = $pgm->pixelArray[$i++];
                }
            }
        }

        return $image;
    }

    /**
     * This reads a PGM file from a given filename and returns the image.
     *
     * @param resource $filename
     *
     * @return Image
     * @throws \Exception
     */
    public function readPGMFile2($filename)
    {
        // The "b" option is for binary input, which is needed if this is compiled under Windows.
        // It has no effect in Linux.
        $file = fopen($filename, 'rb');
        if (!$file) {
            throw new \Exception('Could not open file: ' . $filename, 404);
        }

        return $this->readPGM($file);
    }

    /**
     * Read a PGM file from the given file pointer and return it as a
     * float Image structure with pixels in the range [0,1].
     * If the file contains more than one image, then the images will
     * be returned linked by the "next" field of the Image data structure.
     * See "man pgm" for details on PGM file format.
     * This handles only the usual 8-bit "raw" PGM format.
     * Use xv or the PNM tools (such as pnmdepth) to convert from other formats.
     *
     * @param resource $fp
     *
     * @return Image
     * @throws \Exception
     */
    private function readPGM($fp)
    {
        $file = fread($fp, 8192);
        $lines = explode("\n", $file);
        array_filter($lines);

        $width = $height = $max = 0;

        rewind($fp);
        $char1 = fgetc($fp); // P
        $char2 = fgetc($fp); // 5
//        $this->skipComments($fp);
        fgets($fp); // Discard exactly one byte
//        $this->skipComments($fp);
        list($width, $height) = fscanf($fp, "%d %d");
//        $this->skipComments($fp);
        list($max) = fscanf($fp, "%d");

        if ($char1 != 'P' || $char2 != '5' || $width == 0 || $height == 0 || $max > 255) {
            throw new \Exception('Input is not a standard raw 8-bit PGM file.' . PHP_EOL . 'Use xv or pnmdepth to convert file to 8-bit PGM format.' . PHP_EOL);
        }

        fgetc($fp); // Discard exactly one byte after header.

        // Create floating point image with pixels in range [0,1].
        $image = $this->createImage($height, $width);
        for ($r = 0; $r < $height; $r++) {
            for ($c = 0; $c < $width; $c++) {
                if (!isset($image->pixels[$r])) {
                    $image->pixels[$r] = [ ];
                }
                if (!isset($image->pixels[$r][$c])) {
                    $char = fgetc($fp);
                    $image->pixels[$r][$c] = ((float) $char) / 255.0;
                }
            }
        }

//        $this->skipComments($fp);
        /*        if (getc($fp) == 'P') {
                    ungetc('P', $fp);
                    $image->next = $this->readPGM($fp);
                }*/

        return $image;
    }

    /**
     * PGM files allow a comment starting with '#' to end-of-line.
     * Skip white space including any comments.
     *
     * @param $fp
     */
    function skipComments($fp)
    {
        fscanf($fp, '%[^ ]'); // Skip white space.
        while (!feof($fp)) {
            $line = fgets($fp, 4096);
            if ($line[0] != '#') {

            }
        }
        /*while (($ch = fgetc($fp)) == '#') {
//            while (($ch = fgetc($fp)) != '\n' && $ch != EOF) {
            while (!feof($fp) && ($ch = fgetc($fp)) != PHP_EOL) {
            }
            fscanf($fp, " ");
        }
        ungetc($ch, $fp); // Replace last character read.*/
    }

    /**
     * This writes a PGM file from a given filename and returns the image.
     *
     * @param string $filename
     * @param Image  $image
     *
     * @return Image
     * @throws \Exception
     */
    public function writePGMFile($filename, Image $image)
    {
        $pixels = [ ];
        for ($r = 0; $r < $image->rows; $r++) {
            for ($c = 0; $c < $image->cols; $c++) {
                $pixels[] = $image->pixels[$r][$c];
            }
        }
        $pgm = new PGM();
        $pgm->height = $image->rows;
        $pgm->width = $image->cols;
        $pgm->pixelArray = $pixels;
        $pgm->savePGM($filename);
    }

    /**
     * This writes a PGM file from a given filename and returns the image.
     *
     * @param string $filename
     * @param Image  $image
     *
     * @return Image
     * @throws \Exception
     */
    public function writePGMFile2($filename, Image $image)
    {
        $pixels = [ ];
        for ($r = 0; $r < $image->rows; $r++) {
            for ($c = 0; $c < $image->cols; $c++) {
                $pixels[] = $image->pixels[$r][$c];
            }
        }
        $pgm = new PGM();
        $pgm->height = $image->rows;
        $pgm->width = $image->cols;
        $pgm->pixelArray = $pixels;
        $pgm->savePGM($filename);

        return;
        /*
                // The "b" option is for binary input, which is needed if this is compiled under Windows.
                // It has no effect in Linux.
                $fp = fopen($filename, 'w+');
                if (!$fp) {
                    throw new \Exception('Could not open file: ' . $filename, 404);
                }

                fprintf($fp, "P5\n%d %d\n255\n", $image->cols, $image->rows);

                for ($r = 0; $r < $image->rows; $r++) {
                    for ($c = 0; $c < $image->cols; $c++) {
                        $val = intval(255.0 * $image->pixels[$r][$c]);
        //                fputc(max(0, min(255, $val)), $fp);
                        fwrite($fp, max(0, min(255, $val)));
                    }
                }*/
    }

    /**
     * Draw a white line from (r1,c1) to (r2,c2) on the image.
     * Both points must lie within the image.
     *
     * @param Image $image
     * @param int   $r1
     * @param int   $c1
     * @param int   $r2
     * @param int   $c2
     */
    public function drawLine(Image $image, $r1, $c1, $r2, $c2)
    {
        if ($r1 == $r2 && $c1 == $c2) {
            // Line of zero length.
            return;
        }

        // Is line more horizontal than vertical?
        if (abs($r2 - $r1) < abs($c2 - $c1)) {
            // Put points in increasing order by column.
            if ($c1 > $c2) {
                /*$temp = $r1;
                $r1 = $r2;
                $r2 = $temp;*/
                self::swapValues($r1, $r2);
                /*$temp = $c1;
                $c1 = $c2;
                $c2 = $temp;*/
                self::swapValues($c1, $c2);
            }
            $dr = $r2 - $r1;
            $dc = $c2 - $c1;
            for ($i = $c1; $i <= $c2; $i++) {
                $image->pixels[$r1 + ($i - $c1) * $dr / $dc][$i] = 1.0;
            }
        } else {
            if ($r1 > $r2) {
                /*$temp = $r1;
                $r1 = $r2;
                $r2 = $temp;*/
                self::swapValues($r1, $r2);
                /*$temp = $c1;
                $c1 = $c2;
                $c2 = $temp;*/
                self::swapValues($c1, $c2);
            }
            $dr = $r2 - $r1;
            $dc = $c2 - $c1;
            for ($i = $r1; $i <= $r2; $i++) {
                $image->pixels[$i][$c1 + ($i - $r1) * $dc / $dr] = 1.0;
            }
        }
    }

    function swapValues(&$a, &$b)
    {
        $tmp = $a;
        $a = $b;
        $b = $tmp;
    }
}
