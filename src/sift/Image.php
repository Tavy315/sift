<?php
namespace Sift;

/**
 * Class Image
 *
 * Data structure for a float image.
 *
 * @package Sift
 * @author  Octavian Matei <octav@octav.name>
 */
class Image
{
    /**
     * Dimensions of image.
     *
     * @var int
     */
    public $rows, $cols = 0;

    /**
     * 2D array of image pixels.
     * float **pixels;
     *
     * @var array
     */
    public $pixels = [ [ ] ];

    /**
     * Pointer to next image in sequence.
     * struct ImageSt *next;
     *
     * @var Image
     */
    public $next = null;

    /**
     * @return Image
     */
    public function getNext()
    {
        return $this->next;
    }
}
