<?php
namespace Sift;

/**
 * Class Keypoint
 *
 * Data structure for a keypoint.
 * Lists of keypoints are linked by the "next" field.
 *
 * @package Sift
 * @author  Octavian Matei <octav@octav.name>
 */
class Keypoint
{
    /**
     * Subpixel loction of keypoint (row/col)
     *
     * @var float
     */
    public $row, $col;

    /**
     * Scale and orientation (range [-PI,PI])
     *
     * @var float
     */
    public $scale, $ori;

    /**
     * Vector of descriptor values
     * unsigned char *descrip;
     *
     * @var array
     */
    public $descriptors = [ ];

    /**
     * Pointer to next keypoint in list.
     * struct KeypointSt *next;
     *
     * @var Keypoint
     */
    public $next = null;

    /**
     * @return array
     */
    public function getDescriptors()
    {
        return $this->descriptors;
    }

    /**
     * @param int $descriptor
     */
    public function setDescriptors($index, $descriptor)
    {
        $this->descriptors[$index] = intval($descriptor) % 255;
    }

    /**
     * @return Keypoint
     */
    public function getNext()
    {
        return $this->next;
    }

    /**
     * @param Keypoint $next
     */
    public function setNext($next)
    {
        $this->next = $next;
    }
}
