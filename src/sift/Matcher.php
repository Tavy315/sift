<?php
/**
 * Invariant keypoint matching
 * This file contains a sample program to read images and keypoints, then draw lines connecting matched keypoints.
 *
 * @author David Lowe
 */
namespace Sift;

use Sift\Image;
use Sift\Keypoint;

/**
 * Class Matcher
 *
 * @todo    use Online Image Converter or implement an alternative
 * @link    http://ziin.pl/en/utilities/convert/png2pgm
 *
 * @package Sift
 * @author  Octavian Matei <octav@octav.name>
 */
class Matcher
{
    /**
     * Parse command line arguments and read given files.
     * The command line must specify two input images and two files
     * of keypoints using command line arguments as follows:
     *
     * @usage
     * match -im1 i1.pgm -k1 k1.key -im2 i2.pgm -k2 k2.key > result.v
     *
     * @param null $imageFile1
     * @param null $keyFile1
     * @param null $imageFile2
     * @param null $keyFile2
     *
     * @throws \Exception
     */
    public function match($imageFile1 = null, $keyFile1 = null, $imageFile2 = null, $keyFile2 = null)
    {
        if ($imageFile1 === null || $imageFile2 === null || $keyFile1 === null || $keyFile2 === null) {
            throw new \Exception('Command line does not specify all image and keypoint files.');
        }
        $util = new Util();
//        $image1 = $util->readPGMFile($imageFile1);
        $keypoints1 = $util->readKeyFile($keyFile1);
//        $image2 = $util->readPGMFile($imageFile2);
        $keypoints2 = $util->readKeyFile($keyFile2);

        $this->calculateMatches($keypoints1, $keypoints2);
    }

    public function calculateMatches(Keypoint $keys1, Keypoint $keys2)
    {
        $count = 0;
        // Match the keys in list keys1 to their best matches in keys2.
        for ($k = $keys1; $k !== null; $k = $k->getNext()) {
            $match = $this->checkForMatch($k, $keys2);
            if ($match !== null) {
                $count++;
            }
        }

        echo 'Found ' . $count . ' matches' . PHP_EOL;
    }

    /**
     * Given a pair of images and their keypoints, pick the first keypoint
     * from one image and find its closest match in the second set of keypoints.
     * Then write the result to a file.
     *
     * @param Image    $image1
     * @param Keypoint $keys1
     * @param Image    $image2
     * @param Keypoint $keys2
     */
    public function findMatches(Image $image1, Keypoint $keys1, Image $image2, Keypoint $keys2)
    {
        /**
         * Create a new image that joins the two images vertically.
         *
         * @var Image $result
         */
        $result = $this->combineImagesVertically($image1, $image2);
        $lib = new Util();

        $count = 0;
        // Match the keys in list keys1 to their best matches in keys2.
        for ($k = $keys1; $k !== null; $k = $k->getNext()) {
            $match = $this->checkForMatch($k, $keys2);

            // Draw a line on the image from keys1 to match.
            // Note that we must add row count of first image to row position in second
            // so that line ends at correct location in second image.
            if ($match !== null) {
                $count++;
                $lib->drawLine($result, (int) $k->row, (int) $k->col, (int) ($match->row + $image1->rows), (int) $match->col);
            }
        }
        /*
                $fp = fopen('out.pgm', 'w+');
                // Write result image to standard output.
                $lib->writePGM($fp, $result);*/
        echo 'Found ' . $count . ' matches' . PHP_EOL;
    }

    /**
     * This searches through the keypoints in kpList for the two closest matches to key.
     * If the closest is less than 0.6 times distance to second closest,
     * then return the closest match. Otherwise, return NULL.
     *
     * @param Keypoint $key
     * @param Keypoint $kpList
     *
     * @return null|Keypoint
     */
    private function checkForMatch(Keypoint $key, Keypoint $kpList)
    {
        $squareDistance1 = $squareDistance2 = 100000000;
        $minKey = null;

        // Find the two closest matches, and put their squared distances in $squareDistance1 and $squareDistance2.
        for ($k = $kpList; $k !== null; $k = $k->getNext()) {
            $squareDistance = $this->squareDistance($key, $k);

            if ($squareDistance < $squareDistance1) {
                $squareDistance2 = $squareDistance1;
                $squareDistance1 = $squareDistance;
                $minKey = $k;
            } elseif ($squareDistance < $squareDistance2) {
                $squareDistance2 = $squareDistance;
            }

            // Check whether closest distance is less than 0.6 of second.
            if ($squareDistance2 < 100000000 && $squareDistance1 < 0.36 * $squareDistance2) {
                return $minKey;
            }
        }

        // Check whether closest distance is less than 0.6 of second.
        if ($squareDistance1 < 0.36 * $squareDistance2) {
            return $minKey;
        }

        return null;
    }

    /** Return a new image that contains the two images with im1 above im2.
     *
     * @param Image $image1
     * @param Image $image2
     *
     * @return Image
     */
    private function combineImagesVertically(Image $image1, Image $image2)
    {
        /** int */
        $rows = $cols = 0;
        $result = new Image();

        $rows = $image1->rows + $image2->rows;
        $cols = max($image1->cols, $image2->cols);

        $lib = new Util();
        $result = $lib->createImage($rows, $cols);

        // Set all pixels to 0,5, so that blank regions are grey.
        for ($r = 0; $r < $rows; $r++) {
            for ($c = 0; $c < $cols; $c++) {
                $result->pixels[$r][$c] = 0.5;
            }
        }

        // Copy images into result.
        for ($r = 0; $r < $image1->rows; $r++) {
            for ($c = 0; $c < $image1->cols; $c++) {
                $result->pixels[$r][$c] = $image1->pixels[$r][$c];
            }
        }

        for ($r = 0; $r < $image2->rows; $r++) {
            for ($c = 0; $c < $image2->cols; $c++) {
                $result->pixels[$r + $image2->rows][$c] = $image2->pixels[$r][$c];
            }
        }

        return $result;
    }

    /**
     * Return squared distance between two keypoint descriptors.
     *
     * @param Keypoint $keypoint1
     * @param Keypoint $keypoint2
     *
     * @return int
     */
    private function squareDistance(Keypoint $keypoint1, Keypoint $keypoint2)
    {
        $pk1 = $keypoint1->getDescriptors();
        $pk2 = $keypoint2->getDescriptors();

        $squareDistance = 0;
        /*
        for ($i = 0; $i < 128; $i++) {
            $squareDistance += pow($pk1[$i] - $pk2[$i], 2);
        }
        */

        /*$powDiff = function($a, $b) {
            return ($a - $b) * ($a - $b);
        };
        $squareDistance = array_sum(array_map($powDiff, $pk1, $pk2));*/

        $squareDistance = array_sum(array_map([ $this, 'powDiff' ], $pk1, $pk2));

        return $squareDistance;
    }

    private function powDiff($a, $b)
    {
        return ($a - $b) * ($a - $b);
    }

    /**
     * Return squared distance between two keypoint descriptors.
     *
     * @param Keypoint $keypoint1
     * @param Keypoint $keypoint2
     *
     * @return int
     */
    private function squareDistance2(Keypoint $keypoint1, Keypoint $keypoint2)
    {
        $dif = $squareDistance = 0;
//        unsigned char *pk1, *pk2;

        $pk1 = $keypoint1->getDescriptors();
        $pk2 = $keypoint2->getDescriptors();

        for ($i = 0; $i < 128; $i++) {
//            $dif = (int) *pk1++ - (int) *pk2++; ??
//            $dif = (int) $pk1[$i] - (int) $pk2[$i];
            $dif = $pk1[$i] - $pk2[$i];
            $squareDistance += pow($dif, 2);
        }

        return $squareDistance;
    }
}
